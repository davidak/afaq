# Were any of the Bolshevik oppositions a real alternative?

The real limitations in Bolshevism can best be seen by the various oppositions
to the mainstream of that party. That Bolshevik politics were not a suitable
instrument for working class self-liberation can be seen by the limited way
which opposition groups questioned Bolshevik orthodoxy -- even, in the case of
the opposition to the rising Stalinist bureaucracy. Each opposition was
fundamentally in favour of the Bolshevik monopoly of power, basically seeking
reforms on areas which did not question it (such as economic policy). This
does not mean that the various oppositions did not have valid points, just
that they shared most of the key assumptions of Bolshevism which undermined
the Russian revolution either by their application or their use to justify
specific (usually highly authoritarian) practice.

We will not cover all the various oppositions with the Bolshevik party here
(Robert V. Daniels' **The Conscience of the Revolution** discusses all of them
in some detail, as does Leonard Schapiro's **The Origin of the Communist
Autocracy**). We will concentrate on the _"Left Communists"_ of 1918, the
_"Workers' Opposition"_ of 1920/1 and the Trotsky-led "Left Opposition" of
1923-7. It can be said that each opposition is a pale reflection of the one
before it and each had clear limitations in their politics which fatally
undermined any liberatory potential they had. Indeed, by the time of the
_"Left Opposition"_ we are reduced to simply the more radical sounding faction
of the state and party bureaucracy fighting it out with the dominant faction.

To contrast these fake "oppositions" with a genuine opposition, we will
discuss (in [section 4](append45.md#app4)) the _"Workers' Group"_ of 1923
which was expelled from the Communist Party and repressed because it stood for
(at least until the Bolshevik party seized power) traditional socialist
values. This repression occurred, significantly, under Lenin and Trotsky in
1922/3. The limited nature of the previous oppositions and the repression of a
**genuine** dissident working class group within the Communist Party shows how
deeply unlibertarian the real Bolshevik tradition is. In fact, it could be
argued that the fate of all the non-Trotskyist oppositions shows what will
inevitably happen when someone takes the more democratic sounding rhetoric of
Lenin at face value and compares it to his authoritarian practice, namely
Lenin will turn round and say unambiguously that he had already mentioned his
practice before hand and the reader simply had not been paying attention.

## 1 Were the _"Left Communists"_ of 1918 an alternative?

The first opposition of note to Lenin's state capitalist politics was the
_"Left Communists"_ in early 1918. This was clustered around the Bolshevik
leader Bukharin. This grouping was focused around opposition to the Brest-
Litovsk peace treaty with Germany and Lenin's advocacy of _"state capitalism"_
and _"one-man management"_ as the means of both achieving socialism and
getting Russia out of its problems. It is the latter issue that concerns us
here.

The first issue of their theoretical journal **Kommunist** was published in
April 1920 and it argued vigorously against Lenin's advocacy of _"one-man
management"_ and state capitalism for "socialist" Russia. They correctly
argued _"for the construction of the proletarian society by the class
creativity of the workers themselves, not by the Ukases of the captains of
industry . . . If the proletariat itself does not know how to create the
necessary prerequisites for the socialist organisation of labour, no one can
do this for it and no one can compel it to do this. The stick, if raised
against the workers, will find itself in the hands of a social force which is
either under the influence of another social class or is in the hands of the
soviet power; but the soviet power will then be forced to seek support against
the proletariat from another class (e.g. the peasantry) and by this it will
destroy itself as the dictatorship of the proletariat. Socialism and socialist
organisation will be set up by the proletariat itself, or they will not be set
up at all: something else will be set up -- state capitalism."_ [Osinsky,
quoted by Brinton, **The Bolsheviks and Workers' Control**, p. 39]

Lenin reacted sharply, heaping insult upon insult on the Left Communists and
arguing against their ideas on workers' self-management. Rather than see self-
management (or even workers' control) as the key, he argued forcefully in
favour of one-man management and state capitalism as both the means of solving
Russia's immediate problems **and** building socialism. Moreover, he linked
this with his previous writings, correctly noting his _"'high' appreciation of
state"_ had been given _"**before** the Bolsheviks seized power."_ For Lenin,
_"Socialism [was] inconceivable without large scale capitalist engineering . .
. [and] without planned state organisation, which keeps tens of millions of
people to the strictest observance of a unified standard in production and
distribution."_ Thus _"our task is to study the state capitalism of the
Germans, to spare **no effort** in copying it and not shrink from adopting
**dictatorial** methods to hasten the copying of it."_ [**Selected Works**,
vol. 2, p. 636 and p. 635] This required appointing capitalists to management
positions, from which the vanguard could learn.

So, as long as a workers' party held power, the working class need not fear
_"state capitalism"_ and the lack of economic power at the point of
production. Of course, without economic power, working class political power
would be fatally undermined. In practice, Lenin simply handed over the
workplaces to the state bureaucracy and created the social relationships which
Stalinism thrived upon. Unfortunately, Lenin's arguments carried the day (see
see [section 9](append41.md#app9) of the appendix ["What happened during the
Russian Revolution?"](append41.md)). How this conflict was resolved is
significant, given that the banning of factions (which is generally seen as a
key cause in the rise of Stalinism) occurred in 1921 (a ban, incidentally,
Trotsky defended throughout the 1920s). As one historian notes:

> _ "The resolution of the party controversy in the spring of 1918 set a
pattern that was to be followed throughout the history of the Communist
Opposition in Russia. This was the settlement of the issues not by discussion,
persuasion, or compromise, but by a high-pressure campaign in the party
organisations, backed by a barrage of violent invective in the party press and
in the pronouncements of the party leaders. Lenin's polemics set the tone, and
his organisational lieutenants brought the membership into line."_ [Daniels,
**Op. Cit.**, p. 87]

Indeed, _"[s]oon after the party congress had approved the peace [in the
spring of 1918], a Petrograd city party conference produced a majority for
Lenin. It ordered the suspension of the newspaper **Kommunist** which had been
serving as a Left Communist organ . . . The fourth and final issue of the
Moscow **Kommunist** had to be published as a private factional paper rather
than as the official organ of a party organisation."_ Ultimately, _"[u]nder
the conditions of party life established by Lenin, defence of the Opposition
position became impossible within the terms of Bolshevik discipline."_ [**Op.
Cit.**, p. 88 and p. 89] So much for faction rights -- three years **before**
they were officially prohibited in the 10th Party Congress!

However, the _"Left Communists,"_ while correct on socialism needing workers'
economic self-management, were limited in other ways. The major problems with
the _"Left Communists"_ were two-fold.

Firstly, by basing themselves on Bolshevik orthodoxy they allowed Lenin to
dominate the debate. This meant that their more _"libertarian"_ reading of
Lenin's work could be nullified by Lenin himself pointing to the authoritarian
and state capitalist aspects of those very same works. Which is ironic, as
today most Leninists tend to point to these very same democratic sounding
aspects of Lenin's ideas while downplaying the more blatant anti-socialist
ones. Given that Lenin had dismissed such approaches himself during the debate
against the Left Communists in 1918, it seems dishonest for his latter day
followers to do this.

Secondly, their perspective on the role of the party undermined their
commitment to true workers' power and freedom. This can be seen from the
comments of Sorin, a leading Left Communist. He argued that the Left
Communists were _"the most passionate proponents of soviet power, but . . .
only so far as this power does not degenerate . . . in a petty-bourgeois
direction."_ [quoted by Ronald I. Kowalski, **The Bolshevik Party in
Conflict**, p. 135] For them, like any Bolshevik, the party played the key
role. The only true bastion of the interests of the proletariat was the party
which _"is in every case and everywhere superior to the soviets . . . The
soviets represent labouring democracy in general; and its interest, and in
particular the interests of the petty bourgeois peasantry, do not always
coincide with the interests of the proletariat."_ [quoted by Richard Sakwa,
**Soviet Communists in Power**, p. 182] This support for party power can also
be seen in Osinsky's comment that _"soviet power"_ and the "dictatorship of
the proletariat" could _"seek support"_ from other social classes, so showing
that the class did not govern directly.

Thus soviet power was limited to approval of the party line and any deviation
from that line would be denounced as _"petty bourgeois"_ and, therefore,
ignored. _"Ironically,"_ the historian Kowalski notes, _"Sorin's call for a
revived soviet democracy was becoming vitiated by the dominant role assigned,
in the final analysis, to the party."_ [**Op. Cit.**, p. 136] Thus their
politics were just as authoritarian as the mainstream Bolshevism they attacked
on other issues:

> _ "Ultimately, the only criterion that they appeared able to offer was to
define 'proletarian' in terms of adherence to their own policy prescriptions
and 'non-proletarian' by non-adherence to them. In consequence, all who dared
to oppose them could be accused either of being non-proletarian, or at the
very least suffering from some form of 'false consciousness' -- and in the
interests of building socialism must recant or be purged from the party.
Rather ironically, beneath the surface of their fine rhetoric in defence of
the soviets, and of the party as 'a forum for all of proletarian democracy,'
there lay a political philosophy that was arguably as authoritarian as that of
which they accused Lenin and his faction."_ [Kowalski, **Op. Cit.**, pp.
136-7]

This position can be traced back to the fundamentals of Bolshevism (see
[section H.5](secH5.md) on vanguardism). _"According to the Left Communists,
therefore,"_ notes Richard Sakwa, "the party was the custodian of an interest
higher than that of the soviets. Earlier theoretical considerations on the
vanguard role of the party, developed in response to this problem, were
confirmed by the circumstances of Bolshevism in power. The political dominance
of the party over the soviets encouraged an administrative one as well. Such a
development was further encouraged by the emergence of a massive and unwieldy
bureaucratic apparatus in 1918 . . . The Left Communists and the party
leadership were therefore in agreement that . . . the party should play a
tutelary role over the soviets." Furthermore, _"[w]ith such a formulation it
proved difficult to maintain the vitality of the soviet plenum as the soviet
was controlled by a party fraction, itself controlled by a party committee
outside the soviet."_ [**Op. Cit.**, p. 182 and p. 182-3]

With this ideological preference for party power and the ideological
justification for ignoring soviet democracy, it is doubtful that their
(correct) commitment to workers' economic self-management would have been
successful. An economic democracy combined with what amounts to a party
dictatorship would be an impossibility that could never work in practice (as
Lenin in 1921 argued against the "Workers' Opposition").

As such, the fact that Bukharin (one time _"Left Communist") "continued to
eulogise the party's dictatorship, sometimes quite unabashedly"_ during and
after the civil war becomes understandable. In this, he was not being extreme:
_"Bolsheviks no longer bothered to disclaim that the dictatorship of the
proletariat as the 'dictatorship of the party.'"_ [Stephen F. Cohen,
**Bukharin and the Bolshevik Revolution**, p. 145 and p. 142] All the leading
Bolsheviks had argued this position for some time (see [section
H.1.2](secH1.md#sech12), for example). Bukharin even went so far as to argue
that _"the watchword"_ taken up by some workers ("even metal workers"!) of
_"For class dictatorship, but against party dictatorship!"_ showed that the
proletariat "was declassed." This also indicated that a _"misunderstanding
arose which threatened the whole system of the proletarian dictatorship."_
[contained in Al Richardson (ed.), **In Defence of the Russian Revolution**,
p. 192] The echoes of the positions argued before the civil war can be seen in
Bukharin's glib comment that proletarian management of the revolution meant
the end of the _"proletarian"_ dictatorship!

Lastly, the arguments of the Left Communists against "one-man management" were
echoed by the Democratic Centralists at the Ninth Party Congress. One member
of this grouping (which included such _"Left Communists"_ as Osinsky) argued
against Lenin's dominate position in favour of appointed managers inside and
outside the party as follows:

> _ "The Central Committee finds that the [local] party committee is a
bourgeois prejudice, is conservatism bordering on the province of treason, and
that the new form is the replacement of party committees by political
departments, the heads of which by themselves replace the elected committees .
. . You transform the members of the party into an obedient gramophone, with
leaders who order: go and agitate; but they haven't the right to elect their
own committee, their own organs. _

>

> _"I then put the question to comrade Lenin: Who will appoint the Central
Committee? You see, there can be individual authority here as well. Here also
a single commander can be appointed."_ [Sapronov, quoted by Daniels, **Op.
Cit.**, p. 114]

Obviously a man before his time. As Stalin proved, if one-man management was
such a good idea then why wasn't it being practised in the Council of People's
Commissars. However, we should not be surprised by this party regime. After
all, Trotsky had imposed a similar regime in the Army in 1918, as had Lenin in
industry in the same year. As discussed in [section 3](append43.md#app3) of
the appendix [ "What caused the degeneration of the Russian
Revolution?"](append43.md), the Bolshevik preference for centralised
"democracy" effectively hollowed out the real democracy at the base which
makes democracy more than just picking masters.

## 2 What were the limitations of the _"Workers' Opposition"_ of 1920?

The next major opposition group were the _"Workers' Opposition"_ of 1920 and
early 1921. Significantly, the name "Workers' Opposition" was the label used
by the party leadership to describe what latter became a proper grouping
within the party. This group was more than happy to use the label given to it.
This group is generally better known than other oppositions simply because it
was the focus for much debate at the tenth party congress and its existence
was a precipitating factor in the banning of factions within the Communist
Party.

However, like the _"Left Communists,"_ the _"Workers' Opposition"_ did not
extend their economic demands to political issues. Unlike the previous
opposition, however, their support for party dictatorship was more than
logically implied, it was taken for granted. Alexandra Kollontai's pamphlet,
for example, expounding the position of the _"Workers' Opposition"_ fails to
mention political democracy at all, instead discussing exclusively economic
and party democracy. Thus it was a case of the _"Workers' Opposition"_
expressing the _"basis on which, in its opinions, the dictatorship of the
proletariat must rest in the sphere of industrial reconstruction."_ Indeed,
the _"whole controversy boils down to one basic question: who shall build the
communist economy, and how shall it be build?"_ [**Selected Writings of
Alexandra Kollontai**, p. 161 and p. 173]

Kollontai was right to state that the working class _"can alone by the creator
of communism"_ and to ask the question of _"shall we achieve communist through
the workers or over their heads, by the hands of Soviet officials."_ As she
argued, _"**it is impossible to decree communism.**"_ However, her list of
demand were purely economic in nature and she wondered _"[w]hat shall we do
then in order to destroy bureaucracy in the party and replace it by workers'
democracy?"_ She stressed that the _"Workers' Opposition"_ struggle was _"for
establishing democracy in the party, and for the elimination of all
bureaucracy."_ [**Op. Cit.**, p. 176, p. 174, p. 187, p. 192 and p. 197] Thus
her demands were about the internal regime of the party, **not** a call for
wider democratic reforms in the state or society as a whole.

As one historian notes, the _"arguments of Kollontai were . . . strictly
limited in their appeal to the communist party . . . Nor did they in any form
criticise the domination of the communist minority over the majority of the
proletariat. The fundamental weakness of the case of the Workers' Opposition
was that, while demanding more freedom of initiative for the workers, it was
quite content to leave untouched the state of affairs in which a few hundred
thousand imposed their will on many millions. 'And since when have we [the
Workers' Opposition] been enemies of **komitetchina** [manipulation and
control by communist party committees], I should like to know?' Shlyapnikov
asked at the Tenth Party Congress. He went on to explain that the trade union
congress in which, as he and his followers proposed, all control of industry
should be vested would 'of course' be composed of delegates nominated and
elected 'through the party cells, as we always do.' But he argued that the
local trade union cells would ensure the election of men qualified by
experience and ability in pace of those who are 'imposed on us at present' by
the centre. Kollontai and her supporters had no wish to disturb the communist
party's monopoly of political power."_ [Leonard Schapiro, **The Origin of the
Communist Autocracy**, p. 294]

Even this extremely limited demand for more economic democracy were too much
for Lenin. In January, 1921, Lenin was arguing that the Bolsheviks had to
_"add to our platform the following: we must combat the ideological confusion
of those unsound elements of the opposition who go to the lengths of
repudiating all 'militarisation of economy,' of repudiating not only the
'method of appointing' which has been the prevailing method up to now, but
**all** appointments. In the last analysis this means repudiating the leading
role of the Party in relation to the non-Party masses. We must combat the
syndicalist deviation which will kill the Party if it is not completely cured
of it."_ Indeed, _"the syndicate deviation leads to the fall of the
dictatorship of the proletariat."_ [quoted by Brinton, **Op. Cit.**, pp. 75-6]
Maurice Brinton correctly notes that by this Lenin meant that _"working class
power ('the dictatorship of the proletariat') is impossible if there are
militants in the Party who think the working class should exert more power in
production ('the syndicalist deviation')."_ Moreover, _"Lenin here poses quite
clearly the question of 'power of the Party' or 'power of the class.' He
unambiguously opts for the former -- no doubt rationalising his choice by
equating the two. But he goes even further. He not only equates 'workers
power' with the rule of the Party. He equates it with acceptance of the ideas
of the Party leaders!"_ [**Op. Cit.**, p. 76]

At the tenth party congress, the _"Workers' Opposition"_ were labelled _
"petty-bourgeois,"_ _"syndicalist"_ and even _"anarchist"_ simply because they
called for limited participation by workers in the rebuilding of Russia. The
group was _"caused in part by the entry into the ranks of the Party of
elements which had still not completely adopted the communist world view."_
Significantly, those who **had** the _"communist world view"_ did not really
debate the issues raised and instead called the opposition "genuinely counter-
revolutionary," _"objectively counter-revolutionary"_ as well as _"too
revolutionary."_ [quoted by Brinton, **Op. Cit.**, p. 79]

For Lenin, the idea of industrial democracy was a nonsense. In this he was
simply repeating the perspective he had held from spring 1918. As he put it,
it was _"a term that lends itself to misinterpretations. It may be read as a
repudiation of dictatorship and individual authority."_ Industry, he argued,
_"is indispensable, democracy is not"_ and _"on no account must we renounce
dictatorship either." _ Indeed, _"[i]ndustry is indispensable, democracy is a
category proper only to the political sphere"."_ He did admit _"[t]hat [the
opposition] has been penetrating into the broad masses is evident"_ however it
was the duty of the party to ignore the masses. The _"bidding for or
flirtation with the non-Party masses"_ was a _"radical departure from
Marxism."_ _"Marxism teaches,"_ Lenin said, _"and this tenet has not only been
formally endorsed by the whole Communist International in the decisions of the
Second (1920) Congress of the Comintern on the role of the political party of
the proletariat, but has also been confirmed in practice by our revolution --
that only the political party of the working class, i.e. the Communist Party,
is capable of uniting, training and organising a vanguard of the proletariat .
. . . that alone will be capable of withstanding the inevitable petty-
bourgeois vacillation of this mass . . . Without this the dictatorship of the
proletariat is impossible."_ [**Collected Works**, vol. 31, p. 82, p. 27, p.
26, p. 197 and p. 246] In other words, _"Marxism"_ teaches that workers'
democracy and protest (the only means by which _"vacillation"_ can be
expressed) is a danger to the _"dictatorship of the proletariat"_! (see also
[section H.5.3](secH5.md#sech53) on why this position is the inevitable
outcome of vanguardism).

It should be stresses that this opposition and the debate it provoked occurred
after the end of the Civil War in the west. The Whites under Wrangel had been
crushed in November, 1920, and the Russian revolution was no longer in
immediate danger. As such, there was an opportunity for constructive activity
and mass participation in the rebuilding of Russia. The leading Bolsheviks
rejected such demands, even in the limited form advocated by the _"Workers'
Opposition."_ Lenin and Trotsky clearly saw **any** working class
participation as a danger to their power. Against the idea of economic
participation under Communist control raised by the _"Workers' Opposition,"_
the leading Bolsheviks favoured the NEP. This was a return to the same kind of
market-based _"state capitalist"_ strategy Lenin had advocated against the
"Left Communists" **before** the outbreak of the civil war in May 1918 (and,
as noted, he had argued for in 1917). This suggests a remarkable consistency
in Lenin's thoughts, suggesting that claims his policies he advocated and
implemented in power were somehow the opposite of what he _"really"_ wanted
are weak.

As with the "Left Communists" of 1918, Lenin saw his opposition to the
"Workers' Opposition" as reflecting the basic ideas of his politics. _"If we
perish,"_ he said privately at the time according to Trotsky, _"it is all the
more important to preserve our ideological line and give a lesson to our
continuators. This should **never** be forgotten, even in **hopeless**
circumstances."_ [quoted by Daniels, **Op. Cit.**, p. 147]

In summary, like the _"Left Communists"_, the _"Workers' Opposition"_
presented a platform of economic demands rooted in the assumption of Bolshevik
party domination. It is, therefore, unsurprising that leading members of the
_"Workers' Opposition"_ took part in the attack on Kronstadt and that they
wholeheartedly rejected the consistent demands for political **and** economic
that the Kronstadt rebels had raised (see appendix ["What was the Kronstadt
Rebellion?"](append42.md) for more information). Such a policy would be too
contradictory to be applied. Either the economic reforms would remain a dead
letter under party control or the economic reforms would provoke demands for
political change. This last possibility may explain Lenin's vitriolic attacks
on the _"Workers' Opposition."_

This opposition, like the _"Left Communists"_ of 1918, was ultimately defeated
by organisational pressures within the party and state. Victor Serge _"was
horrified to see the voting rigged for Lenin's and Zinoviev's 'majority'"_ in
late 1920. [**Memoirs of a Revolutionary**, p. 123] Kollantai complained that
while officially one and a half million copies of the _"Workers' Opposition"_
manifesto was published, in fact only 1500 were _"and that with difficulty."_
[quoted by Schaprio, **Op. Cit.**, p. 291] This applied even more after the
banning of factions, when the party machine used state power to break up the
base of the opposition in the trade unions as well as its influence in the
party.

_"Victimisation of supporters of the Workers' Opposition,"_ notes Schapiro,
_"began immediately after the Tenth Party Congress. 'The struggle,' as
Shlyapnikov later recounted, 'took place not along ideological lines but by
means . . . of edging out from appointments, of systematic transfers from one
district to another, and even expulsion from the party.' . . . the attack was
levelled not for heretical opinions, but for criticism of any kind of party
shortcomings. 'Every member of the party who spoke in defence of the
resolution on workers' democracy [in the party -- see next section] was
declared a supporter of the Workers' Opposition and guilty of disintegrating
the party,' and was accordingly victimised."_ [**Op. Cit.**, pp. 325-6] Thus
_"the party Secretariat was perfecting its technique of dealing with
recalcitrant individuals by the power of removal and transfer, directed
primarily at the adherents of the Workers' Opposition. (Of the 37 Workers'
Opposition delegates to the Tenth Congress whom Lenin consulted when he was
persuading Shlyapnikov and Kutuzov to enter the Central Committee, only four
managed to return as voting delegates to the next congress.)"_ [Daniels, **Op.
Cit.**, p. 161]

A similar process was at work in the trade unions. For example, _"[w]hen the
metalworkers' union held its congress in May 1921, the Central Committee of
the party handed it a list of recommended candidates for the union leadership.
The metalworkers' delegates voted down the party-backed list, but this gesture
proved futile: the party leadership boldly appointed their own men to the
union offices."_ This was _"a show of political force"_ as the union was a
centre of the Workers' Opposition. [Daniels, **Op. Cit.**, p. 157]

This repression was practised under Lenin and Trotsky, using techniques which
were later used by the Stalinists against Trotsky and his followers. Lenin
himself was not above removing his opponents from the central committee by
undemocratic methods. At the Tenth Party Congress he had persuaded Shlyapnikov
to be elected to the Central Committee in an attempt to undermine the
opposition. A mere _"five months later, Lenin was demanding his expulsion for
a few sharp words of criticism of the bureaucracy, uttered at a private
meeting of a local party cell. If he was looking for a pretext, he could
scarcely have picked a weaker one."_ [Schapiro, **Op. Cit.**, p. 327] Lenin
failed by only one vote short of the necessary two thirds majority of the
Committee.

In summary, the _"Workers' Opposition"_ vision was limited. Politically, it
merely wanted democracy within the party. It did not question the party's
monopoly of power. As such, it definitely did not deserve the labels
_"anarchist"_ and _"syndicalist"_ which their opponents called them. As far as
its economic policy goes, it, too, was limited. Its demands for economic
democracy were circumscribed by placing it under the control of the communist
cells within the trade unions.

However, Kollontai was right to state that only the working class _"can alone
by the creator of communism,"_ that it was impossible to _"achieve communist .
. . over [the workers'] heads, by the hands of Soviet officials"_ and that
_"**it is impossible to decree communism.**"_ As Kropotkin put it decades
before:

> _ "Communist organisation cannot be left to be constructed by legislative
bodies called parliaments, municipal or communal council. It must be the work
of all, a natural growth, a product of the constructive genius of the great
mass. Communism cannot be imposed from above."_ [**Kropotkin's Revolutionary
Pamphlets**, p. 140]

## 3 What about Trotsky's _"Left Opposition"_ in the 1920s?

Finally, there is Trotsky's opposition between 1923 and 1927. Since 1918
Trotsky had been wholeheartedly in favour of the party dictatorship and its
economic regime. This position started to change once his own power came under
threat and he suddenly became aware of the necessity for reform.
Unsurprisingly, his opposition was the last and by far the weakest
politically. As Cornelius Castoriadis points out:

> _ "From the beginning of 1918 until the banning of factions in March 1921,
tendencies within the Bolshevik party were formed that, with farsightedness
and sometimes an astonishing clarity, expressed opposition to the Party's
bureaucratic line and to its very rapid bureaucratisation. These were the
'Left Communists' (at the beginning of 1918), then the 'Democratic Centralist'
tendency (1919), and finally the 'Workers' Opposition' (1920-21). . . these
oppositions were defeated one by one . . . The very feeble echoes of their
critique of the bureaucracy that can be found later in the (Trotskyist) 'Left
Opposition' after 1923 do not have the same signification. Trotsky was opposed
to the **bad policies** of the bureaucracy and to the excesses of its power.
He never put into question its essential nature. Until practically the end of
his life, he never brought up the questions raised by the various oppositions
of the period from 1918 to 1921 (in essence: 'Who manages production?' and
'What is the proletariat supposed to do during the 'dictatorship of the
proletariat,' other than work and follow the orders of 'its' party?')."_
[**Political and Social Writings**, vol. 3, p. 98]

While the _"Left Communists"_ and _"Workers' Opposition"_ had challenged
Lenin's state capitalist economic regime while upholding the Bolshevik
monopoly of power (implicitly or explicitly), Trotsky did not even manage
that. His opposition was firmly limited to internal reforms to the party which
he hoped would result in wider participation in the soviets and trade unions
(he did not bother to explain why continuing party dictatorship would
reinvigorate the soviets or unions).

Politically, Trotsky was unashamedly in favour of party dictatorship. Indeed,
his basic opposition to Stalinism was because he considered it as the end of
that dictatorship by the rule of the bureaucracy. He held this position
consistently during the civil war and into the 1920s (and beyond -- see
[section H.3.8](secH3.md#sech38)). For example, in April 1923, he asserted
quite clearly that _"[i]f there is one question which basically not only does
not require revision but does not so much as admit the thought of revision, it
is the question of the dictatorship of the Party."_ [**Leon Trotsky Speaks**,
p. 158] And was true to his word. In _"The New Course"_ (generally accepted as
being the first public expression of his opposition), he stated that _"[w]e
are the only party in the country, and in the period of the dictatorship it
could not be otherwise."_ Moreover, it was _"incontestable that factions
[within the party] are a scourge in the present situation"_ and so the party
_"does not want factions and will not tolerate them."_ [**The Challenge of the
Left Opposition (1923-25)**, p. 78, p. 80 and p. 86] In May 1924, he even went
so far as to proclaim that:

> _ "Comrades, none of us wishes or is able to be right against his party. The
party in the last analysis is always right, because the party is the sole
historical instrument given to the proletariat for the solution of its basic
problems . . . I know that one cannot be right against the party. It is only
possible to be right with the party and through the party, for history has not
created other ways for the realisation of what is right."_ [quoted by Daniels,
**The Conscience of the Revolution**, p. 240]

However, confusion creeps into the politics of the Left Opposition simply
because they used the term _"workers' democracy"_ a lot. However, a close
reading of Trotsky's argument soon clarifies this issue. Trotsky, following
the Communist Party itself, had simply redefined what _"workers' democracy"_
meant. Rather than mean what you would expect it would mean, the Bolsheviks
had changed its meaning to become _"party democracy."_ Thus Trotsky could talk
about _"party dictatorship"_ and _"workers' democracy"_ without contradiction.
As his support Max Eastman noted in the mid-1920s, Trotsky supported the
_"programme of democracy within the party -- called 'Workers' Democracy' by
Lenin."_ This _"was not something new or especially devised . . . It was part
of the essential policy of Lenin for going forward toward the creation of a
Communist society -- a principle adopted under his leadership at the Tenth
Congress of the party, immediately after the cessation of the civil war."_
[**Since Lenin Died**, p. 35] In the words of historian Robert V. Daniels:

> _ "The Opposition's political ideal was summed up in the slogan 'workers'
democracy,' which referred particularly to two documents [from 1920 and 1923]
. . . Both these statements concerned the need to combat 'bureaucratism' and
implement party democracy."_ [**Op. Cit.**, p. 300]

That this was the case can be seen from the Fourth All-Russian Congress of
Trade Unions in 1921:

> _ "At the meeting of delegates who were party members, Tomsky submitted for
routine approval a set of these on the tasks of trade unions. The approval was
a matter of form, but an omission was noted, The theses made no reference to
the formula of 'proletarian democracy' with which the Tenth Congress had tried
to assuage the rank and file. Riazanov . . . offered an amendment to fill the
breach, in language almost identical with the Tenth Congress resolution: 'The
party must observe with special care the normal methods of proletarian
democracy, particularly in the trade unions, where most of all the selection
of leaders should be done by the organised party masses themselves.' . . . The
party leadership reacted instantaneously to this miscarriage of their plans
for curtailing the idea of union autonomy. Tomksy was summarily ejected from
the trade union congress. Lenin put in appearance together with Bukharin and
Stalin to rectify the unionists' action."_ [Daniels, **Op. Cit.**, p. 157]

The _"New Course Resolution"_ passed in December, 1923, stresses this, stating
that _"Workers' democracy means the liberty of frank discussion of the most
important questions of party life by all members, and the election of all
leading party functionaries and commissions . . . It does not . . . imply the
freedom to form factional groupings, which are extremely dangerous for the
ruling party."_ [Trotsky, **Op. Cit.**, p. 408] It made it clear that
_"workers' democracy"_ was no such thing:

> _ "Worker's democracy signifies freedom of open discussion by all members of
the party of the most important questions of party life, freedom of
controversy about them, and also electiveness of the leading official
individuals and collegia from below upwards. However, it does not at all
suggest freedom of factional groupings . . . It is self-evident that within
the party . . . it is impossible to tolerate groupings, the ideological
contents of which are directed against the party as a whole and against the
dictatorship of the proletariat (such as, for example, the 'Workers' Truth'
and the 'Workers' Group')."_ [quoted by Robert V. Daniels, **Op. Cit.**, p.
222]

As "Left Oppositionist" Victor Serge himself pointed out, _"the greatest reach
of boldness of the Left Opposition in the Bolshevik Party was to demand the
restoration of inner-Party democracy, and it never dared dispute the theory of
single-party government -- by this time, it was too late."_ Trotsky had _"ever
since 1923 [been] for the renovation of the party through inner party
democracy and the struggle against bureaucracy."_ [**The Serge-Trotsky
Papers**, p. 181 and p. 201]

Thus Trotsky's opposition was hardly democratic. In 1926, for example, he took
aim at Stalin's dismissal of the idea of _"the dictatorship of the party"_ as
_"nonsense"_ the previous year. If he were the heroic defender of genuine
workers democracy modern day Trotskyists assert, he would have agreed with
Stalin while exposing his hypocrisy. Instead he defended the concept of _"the
dictatorship of the party"_ and linked it to Lenin (and so Leninist
orthodoxy):

> _ "Of course, the foundation of our regime is the dictatorship of a class.
But this in turn . . . assumes it is class that has come to self-consciousness
through its vanguard, which is to say, through the party. Without this, the
dictatorship could not exist . . . Dictatorship is the most highly
concentrated function of function of a class, and therefore the basic
instrument of a dictatorship is a party. In the most fundamental aspects a
class realises its dictatorship through a party. That is why Lenin spoke not
only of the dictatorship of the class but also the dictatorship of the party
and, **in a certain sense**, made them identical."_ [Trotsky, **The Challenge
of the Left Opposition (1926-27)**, pp. 75-6]

Trotsky argued that Stalin's repudiation of the "dictatorship of the party"
was, in fact, a ploy to substitute the dictatorship of the party _"apparatus"_
for the dictatorship of the party (a theme which would be raised in the
following year's **Platform of the Opposition**). Such a substitution, he
argued, had its roots in a _"disproportion"_ between workers' democracy and
peasants' democracy (or _"the private sector of the economy"_ in general). As
long as there was a _"proper 'proportion'"_ between the two and _"the advance
of democratic methods in the party and working class organisations,"_ then
_"the identification of the dictatorship of the class with that of the party
is fully and completely justified historically and politically."_ Needless to
say, Trotsky did not bother to ask how much democracy (of **any** kind) was
possible under a party dictatorship nor how a class could run society or have
_"democratic"_ organisations if subjected to such a dictatorship. For him it
was a truism that the _"dictatorship of a party does not contradict the
dictatorship of the class either theoretically or practically, but is an
expression of it."_ [**Op. Cit.**, p. 76] Needless to say, the obvious
conclusion to draw from Trotsky's argument is that if a revolution occurred in
a country without a peasantry then the _"dictatorship of the party"_ would be
of no real concern!

This was no temporary (7 year!) aberration. As indicated in [section
H.3.8](secH3.md#sech38), Trotsky repeated this support for party
dictatorship ten years later (and after). Furthermore, Trotsky's defence of
party dictatorship against Stalin was included in the 1927 **Platform of the
Opposition**. This included the same contradictory demands for workers'
democracy and the revitalising of the soviets and trade unions with deeply
rooted ideological support for party dictatorship. This document made his
opposition clear, attacking Stalin for **weakening** the party's dictatorship.
In its words, the _"growing replacement of the party by its own apparatus is
promoted by a 'theory' of Stalin's which denies the Leninist principle,
inviolable for every Bolshevik, that the dictatorship of the proletariat is
and can be realised only through the dictatorship of the party."_ It repeats
this principle by arguing that _"the dictatorship of the proletariat demands a
single and united proletarian party as the leader of the working masses and
the poor peasantry."_ As such, _"[w]e will fight with all our power against
the idea of two parties, because the dictatorship of the proletariat demands
as its very core a single proletarian party. It demands a single party."_
[**The Platform of the Opposition**] Even in the prison camps in the late
1920s and early 1930s, _"almost all the Trotskyists continued to consider that
'freedom of party' would be 'the end of the revolution.' 'Freedom to choose
one's party -- that is Menshevism,' was the Trotskyists' final verdict."_
[Ante Ciliga, **The Russian Enigma**, p. 280]

Once we understand that _"workers' democracy"_ had a very specific meaning to
the Communist Party, we can start to understand such apparently contradictory
demands as the _"consistent development of a workers' democracy in the party,
the trade unions, and the soviets."_ Simply put, this call for _"workers'
democracy"_ was purely within the respective party cells and **not** a call
for **genuine** democracy in the unions or soviets. Such a position in no way
undermines the dictatorship of the party.

Economically, Trotsky's opposition was far more backward than previous
oppositions. For Trotsky, economic democracy was not an issue. It played no
role in determining the socialist nature of a society. Rather state ownership
did. Thus he did not question one-man management in the workplace nor the
capitalist social relationships it generated. For Trotsky, it was _"necessary
for each state-owned factory, with its technical director and with its
commercial director, to be subjected not only to control from the top -- by
the state organs -- but also from below, by the market which will remain the
regulator of the state economy for a long time to come."_ In spite of the
obvious fact that the workers did not control their labour or its product,
Trotsky asserted that _"[n]o class exploitation exists here, and consequently
neither does capitalism exist."_ Moreover, _"socialist industry . . . utilises
methods of development which were invented by capitalist economy."_
Ultimately, it was not self-management that mattered, it was _"the growth of
Soviet state industry [which] signifies the growth of socialism itself, a
direct strengthening of the power of the proletariat"_! [**The First 5 Years
of the Communist International**, vol. 2, p. 237 and p. 245]

Writing in 1923, he argued that the _"system of actual one-man management must
be applied in the organisation of industry from top to bottom. For leading
economic organs of industry to really direct industry and to bear
responsibility for its fate, it is essential for them to have authority over
the selection of functionaries and their transfer and removal."_ These
economic organs must _"in actual practice have full freedom of selection and
appointment."_ He also tied payment to performance (just as he did during the
civil war), arguing that _"the payment of the directors of enterprises must be
made to depend on their balance sheets, like wages depend on output."_ [quoted
by Robert V. Daniels, **A Documentary History of Communism**, vol. 1, p. 237]

Moreover, Trotsky's key idea during the 1920s was to industrialise Russia. As
the 1927 Platform argued, it was a case that the _"present tempo of
industrialisation and the tempo indicated for the coming years are obviously
inadequate"_ and so the _"necessary acceleration of industrialisation"_ was
required. In fact, the _"Soviet Union must nor fall further behind the
capitalist countries, but in the near future must overtake them."_ Thus
industrialisation _"must be sufficient to guarantee the defence of the country
and in particular an adequate growth of war industries."_ [**The Platform of
the Opposition**]

In summary, Trotsky's "opposition" in no way presented any real alternative to
Stalinism. Indeed, Stalinism simply took over and applied Trotsky's demands
for increased industrialisation. At no time did Trotsky question the
fundamental social relationships within Soviet society. He simply wished the
ruling elite to apply different policies while allowing him and his followers
more space and freedom within the party structures. Essentially, as the 1927
Platform noted, he saw Stalinism as the victory of the state bureaucracy over
the party and its dictatorship. Writing ten years after the Platform, Trotsky
reiterated this: _"The bureaucracy won the upper hand. It cowed the
revolutionary vanguard, trampled upon Marxism, prostituted the Bolshevik party
. . . To the extent that the political centre of gravity has shifted form the
proletarian vanguard to the bureaucracy, the party has changed its social
structure as well as its ideology."_ [**Stalinism and Bolshevism**] He simply
wanted to shift the _"political centre of gravity"_ back towards the party, as
it had been in the early 1920s when he and Lenin were in power. He in no
significant way questioned the nature of the regime or the social
relationships it was rooted in.

This explains his continual self-imposed role after his exile of loyal
opposition to Stalinism in spite of the violence applied to him and his
followers by the Stalinists. It also explains the lack of excitement by the
working class over the _"Left Opposition."_ There was really not that much to
choose between the two factions within the ruling party/elite. As Serge
acknowledged: _"Outraged by the Opposition, they [the bureaucrats] saw it as
treason against them; which in a sense it was, since the Opposition itself
belonged to the ruling bureaucracy."_ [**Memoirs of a Revolutionary**, p. 225]

This may come as a shock to many readers. This is because Trotskyists are
notorious for their rewriting of the policies of Trotsky's opposition to the
rise of what became known as Stalinism. This revisionism can take extreme
forms. For example, Chris Harman (of the UK's SWP) in his summary of the rise
Stalinism asserted that after _"Lenin's illness and subsequent death"_ the
_"principles of October were abandoned one by one."_ [**Bureaucracy and
Revolution in Eastern Europe**, p. 14] Presumably, in that case, the
_"principles of October"_ included the practice of, and ideological commitment
to, party dictatorship, one-man management, banning opposition groups/parties
(as well as factions within the Communist Party), censorship, state repression
of working class strikes and protests, piece-work, Taylorism, the end of
independent trade unions and a host of other crimes against socialism
implemented under Lenin and normal practice at the time of his death.

Harman is correct to say that _"there was always an alternative to Stalinism.
It meant, in the late 1920s, returning to genuine workers' democracy and
consciously linking the fate of Russia to the fate of world revolution."_ Yet
this alternative was not Trotsky's. Harman even goes so far as to assert that
the _"historical merit of the Left Opposition"_ was that it _"did link the
question of the expansion of industry with that of working-class democracy and
internationalism."_ [**Op. Cit.**, p. 19]

However, in reality, this was **not** the case. Trotsky, nor the Left
Opposition, supported _"genuine"_ working-class democracy, unless by
_"genuine"_ Harman means _"party dictatorship presiding over."_ This is clear
from Trotsky's writings for the period in question. The Left Opposition did
**not** question the Bolshevik's monopoly of power and explicitly supported
the idea of party dictatorship. This fact helps explains what Harman seems
puzzled by, namely that Trotsky _"continued to his death to harbour the
illusion that somehow, despite the lack of workers' democracy, Russia was a
'workers' state.'"_ [**Op. Cit.**, p. 20] Strangely, Harman does not explain
why Russia was a _"workers' state"_ under Lenin and Trotsky, given its _"lack
of workers' democracy."_ But illusions are hard to dispel, sometimes.

So, for Trotsky, like all leading members of the Communist Party and its
_"Left Opposition"_, _"workers' democracy"_ was **not** considered important
and, in fact, was (at best) applicable only within the party. Thus the
capitulation of many of the Left Opposition to Stalin once he started a policy
of forced industrialisation comes as less of a surprise than Harman seems to
think it was. As Ante Ciliga saw first hand in the prison camps, _"the
majority of the Opposition were . . . looking for a road to reconciliation;
whilst criticising the Five Year Plan, they put stress not on the part of
exploited class played by the proletariat, but on the technical errors made by
the Government **qua** employer in the matter of insufficient harmony within
the system and inferior quality of production. This criticism did not lead to
an appeal to the workers against the Central Committee and against
bureaucratic authority; it restricted itself to proposing amendments in a
programme of which the essentials were approved. The socialist nature of State
industry was taken for granted. They denied the fact that the proletariat was
exploited; for 'we were in a period of proletarian dictatorship.'"_ [**The
Russian Enigma**, p. 213]

As Victor Serge noted, _"[f]rom 1928-9 onwards, the Politbureau turned to its
own use the great fundamental ideas of the now expelled Opposition (excepting,
of course, that of working-class democracy) and implemented them with ruthless
violence."_ While acknowledging that the Stalinists had applied these ideas in
a more extreme form than the Opposition planned, he also acknowledged that
_"[b]eginning in those years, a good many Oppositionists rallied to the
'general line' and renounced their errors since, as they put it, 'After all,
it is our programme that is being applied.'"_ Nor did it help that at _"the
end of 1928, Trotsky wrote to [the Opposition] from his exile . . . to the
effect that, since the Right represented the danger of a slide towards
capitalism, we had to support the 'Centre' -- Stalin -- against it."_ [**Op.
Cit.**, p. 252 and p. 253]

However, Serge's comments on _"working-class democracy"_ are somewhat
incredulous, given that he knew fine well that the Opposition did not stand
for it. His summary of the 1927 Platform was restricted to it aiming _"to
restore life to the Soviets . . . and above all to revitalise the Party and
the trade unions. . . In conclusion, the Opposition openly demanded a Congress
for the reform of the Party, and the implementation of the excellent
resolutions on internal democracy that had been adopted in 1921 and 1923."_
[**Op. Cit.**, pp. 224-5] Which is essentially correct. The Platform was based
on redefining "workers' democracy" to mean "party democracy" within the
context of its dictatorship.

We can hardly blame Harman, as it was Trotsky himself who started the process
of revising history to exclude his own role in creating the evils he
(sometimes) denounced his opponents within the party for. For example, the
1927 Platform states that _"[n]ever before have the trade unions and the
working mass stood so far from the management of socialist industry as now"_
and that _"[p]re-revolutionary relations between foremen and workmen are
frequently found."_ Which is hardly surprising, given that Lenin had argued
for, and implemented, appointed one-man management armed with "dictatorial
powers" from April 1918 and that Trotsky himself also supported one-man
management (see [section 10](append41.md#app10) of the appendix ["What
happened during the Russian Revolution?"](append41.md)).

Even more ironically, Harman argues that the Stalinist bureaucracy became a
ruling class in 1928 when it implemented the first five year plan. This
industrialisation was provoked by military competition with the west, which
forced the _"drive to accumulate"_ which caused the bureaucracy to attack
_"the living standards of peasants and workers."_ He quotes Stalin: _"to
slacken the pace (of industrialisation) would mean to lag behind; and those
who lag behind are beaten . . . We must make good this lag in ten years.
Either we do so or they crush us."_ Moreover, the _"environment in which we
are placed . . . at home and abroad . . . compels us to adopt a rapid rate of
industrialisation."_ [Harman, **Op. Cit.**, pp. 15-6] Given that this was
exactly the same argument as Trotsky in 1927, it seems far from clear that the
_"Left Opposition"_ presented any sort of alternative to Stalinism. After all,
the _"Left Opposition took the stand that large-scale new investment was
imperative, especially in heavy industry, and that comprehensive planning and
new sources of capital accumulation should be employed immediately to effect a
high rate of industrial expansion . . . They also stressed the necessity of
rapidly overtaking the capitalist powers in economic strength, both as a
guarantee of military security and as a demonstration of the superiority of
the socialist system."_ [Robert V. Daniels, **The Conscience of the
Revolution**, p. 290]

Would the Left Opposition's idea of _"primitive socialist accumulation"_ been
obtained by any means other than politically enforced exploitation and the
repression of working class and peasant protest? Of course not. Faced with the
same objective pressures and goals, would it have been any different if that
faction had become dominant in the party dictatorship? It is doubtful, unless
you argue that who is in charge rather than social relationships that
determine the "socialist" nature of a regime. But, then again, that is
precisely what Trotskyists like Harman do do when they look at Lenin's Russia.

As for Harman's assertion that the Left Opposition stood for
_"internationalism,"_ that is less straight forward than he would like. As
noted, it favoured the industrialisation of Russia to defend the regime
against its foreign competitors. As such, the Left Opposition were as
committed to building "socialism" in the USSR as were the Stalinist promoters
of _"socialism in one country."_ The difference was that the Left Opposition
also argued for spreading revolution externally as well. For them, this was
the **only** means of assuring the lasting victory of "socialism" (i.e.
statised industry) in Russia. So, for the Left Opposition, building Russia's
industrial base was part and parcel of supporting revolution internationally
rather, as in the case of the Stalinists, an alternative to it.

The contradictions in Trotsky's position may best be seen from the relations
between Lenin's Russia and the German military. Negotiations between the two
states started as early as 1920 with an important aide of Trotsky's. The fruit
of the German military's negotiations were _"secret military understandings."_
By September 1922 German officers and pilots were training in Russia. An
organisation of German military and industrial enterprises in Russia was
established and under it's auspices shells, tanks and aircraft were
manufactured in Russia for the German army (an attempt to produce poison gas
failed). [E.H. Carr, **The Bolshevik Revolution**, vol. 3, p. 327 and pp.
431-2] In April, 1923, the German High Command ordered 35 million gold marks
worth of war material. [Aberdeen Solidarity, **Spartakism to National
Bolshevism**, p. 24]

These relations had their impact on the politics of the German Communist Party
who enforced its so-called _"Schlageter Line"_ of co-operation with
nationalist and fascist groups. This policy was first promoted in the
Comintern by leading Communist Radek and inspired by Zinoviev. According to
Radek, _"national Bolshevism"_ was required as the _"strong emphasis on the
nation in Germany is a revolutionary act."_ [quoted in E.H. Carr, **The
Interregnum 1923-1924**, p. 177] During the summer of 1923, joint meetings
with them were held and both communist and fascist speakers urged an alliance
with Soviet Russia against the Entente powers. So, for several months, the
German Communists worked with the Nazis, going so as far as to stage rallies
and share podiums together. The Communist leader Ruth Fischer even argued that
_"he who denounces Jewish capital . . . is already a warrior in the class war,
even though he does not know it"_ (she latter said her remarks had been
distorted). [quoted in E.H. Carr, **Op. Cit.**, p. 182f] This continued until
_"the Nazis leadership placed a ban on further co-operation."_ [E.H. Carr,
**Op. Cit.**, p. 183] Thus the activities of the German communists were
tailored to fit into the needs of Lenin's regime and Trotsky played a key role
in the negotiations which started the process.

How _"internationalist"_ was it to arm and train the very forces which had
crushed the German revolutionary workers between 1919 and 1921? How sensible
was it, when pressing for world revolution, to enhance the power of the army
which would be used to attack any revolution in Germany? Which, of course, was
what happened in 1923, when the army repressed the Comintern inspired revolt
in November that year. Trotsky was one of the staunchest in favour of this
insurrection, insisting that it be fixed for the 7th of that month, the
anniversary of the Bolshevik seizure of power. [E.H. Carr, **Op. Cit.**, p.
205] The attempted revolt was a dismal failure. Rather than a revolution in
Berlin on the 7th of November, there was a diner at the Russian embassy for
German officers, industrialists and officials to celebrate the anniversary of
the Russian revolution. [Carr, **Op. Cit.**, p. 226] The big question is how
many Communists and workers killed in the revolt had been at the receiving end
of weapons and training supplied to the German army by Trotsky's Red Army?

Moreover, the **nature** of any such revolution is what counts. The Left
Opposition would have encourage revolutions which followed (to re-quote the
**Platform of the Opposition**) the _"Leninist principle"_ (_"inviolable for
every Bolshevik"_) that _"the dictatorship of the proletariat is and can be
realised only through the dictatorship of the party."_ It would have opposed
workers' self-management in favour of nationalisation and one-man management.
In other words, the influence of the Left Opposition would have been as
detrimental to the global workers' movement and other revolutions as Stalin's
was (or, for that matter, Lenin's) although, of course, in a different way.
Generalising Lenin's state capitalism would not have resulted in socialism, no
matter how many revolutions in the west the Left Opposition encouraged.

Finally, the fate of the _"Left Opposition"_ should be noted. As befell the
previous oppositions, the party machine was used against it. Ironically, the
Stalinists began by using the very techniques the Trotskyists had used against
their opponents years before. For example, the Eighth Party Congress in
December 1919 agreed that _"[a]ll decisions of the higher jurisdiction are
absolutely binding for the lower."_ Moreover, _"[e]ach decision must above all
be fulfilled, and only after this is an appeal to the corresponding party
organ permissible."_ Centralism was reaffirmed: _"The whole matter of
assignment of party workers is in the hands of the Central Committee of the
party. Its decision is binding for everyone..."_ These decisions were used as
a weapon against the opposition: _"Translating this principle into practice,
the Secretariat under Krestinsky [a Trotsky supporter] began deliberately to
transfer party officials for political reasons, to end personal conflicts and
curb opposition."_ In 1923, the Secretariat _"brought into play its power of
transfer, which had already proven to be an effective political weapon against
the Ukrainian Leftists and the Workers' Opposition."_ [Robert V. Daniels,
**Op. Cit.**, p. 113 and p. 229]

The party itself had been reorganised, with _"the replacement of local party
committees, which were at least democratic in form, by bureaucratically
constituted 'political departments.' With the institution of such bodies, all
political activity . . . was placed under rigid control from above. This
innovation was taken from the army; as its origin suggests, it was strictly a
military, authoritarian institution, designed for transmitting propaganda
downward rather than opinion upward."_ [**Op. Cit.**, p. 114] Needless to say,
it was Trotsky himself who implemented that regime in the army to begin with.

It should also be remembered that when, in early in 1922, the _"Workers'
Opposition"_ had appealed to the Communist abroad in the form of a statement
to a Comintern Congress, Trotsky defended the party against its claims. These
claims, ironically, included the accusation that the _"party and trade-union
bureaucracy . . . ignore the decisions of our congresses on putting workers'
democracy [inside the party] into practice."_ Their _"effort to draw the
proletarian masses closer to the state is declared to be 'anarcho-
syndicalism,' and its adherents are subjected to persecution and
discrediting."_ They argued that the _"tutelage and pressure by the
bureaucracy goes so far that it is prescribed for members of the party, under
threat of exclusion and other repressive measures, to elect not those whom the
Communists want themselves, but those whom the ignorant high places want."_
[quoted by Daniels, **Op. Cit.**, p. 162]

Even more ironically, the dominant faction of the bureaucracy heaped upon
Trotsky's opposition faction similar insults to those he (and Lenin) had
heaped upon previous oppositions inside and outside the party. In 1924, the
Trotskyist opposition was accused of having _"clearly violated the decision of
the Tenth Congress . . . which prohibited the formation of factions within the
party"_ and has _"enlivened the hopes of all enemies of the party, including
the West-European bourgeoisie, for a split in the ranks of the Russian
Communist Party."_ In fact, it was a _"direct departure of Leninism"_ and
_"also a clearly expressed **petty-bourgeois deviation**"_ reflecting _"the
pressure of the petty bourgeois on the position of the proletarian party and
its policy."_ [contained in Daniels, **A Documentary History of Communism**,
vol. 1, pp. 247-8] In 1927 the "United Opposition" was _"[o]bjectively . . . a
tool of the bourgeois elements."_ [quoted by Daniels, **The Conscience of the
Revolution**, p. 318]

One of the ways which supporters of Leninism seek to differentiate it from
Stalinism is on the issue of repression within the Communist Party itself.
However, the suppression of opposition currents within Bolshevism did not
start under Stalinism, it had existed to some degree from the start.
Ironically, Trotsky's belated opposition faced exactly the same measures he
had approved for use against groups like the _"Workers' Opposition"_ within a
party regime he himself had helped create.

Of course, the Stalinists did not stop there. Once the "Left Opposition" was
broken its members were brutally repressed. Some were simply murdered, many
more arrested and placed into prison camps where many died. Which shows, in
its own way, a key difference between Lenin's and Stalin's regime. Under
Lenin, the opposition **outside** the party was brutally repressed. Stalin
simply applied the methods used by Lenin outside the party to oppositions
within it.

## 4 What do these oppositions tell us about the essence of Leninism?

The history and ideas of these oppositions are important in evaluating the
claims of pro-Bolsheviks. If, as modern-day supporters of Bolshevism argue,
Leninism is inherently democratic and that before the revolution it stood for
basic civil liberties for the working class then we have to come to the
conclusion that none of the party oppositions represented the _"true"_
Leninist tradition. Given that many Trotskyists support the _"Left
Opposition"_ as the only _"real"_ opposition to Stalin, defending the true
essence of Bolshevism, we can only wonder what the "real" Bolshevik tradition
is. After all, the _"Left Opposition"_ wholeheartedly supported party
dictatorship, remained silent on workers' control and urged the speeding up of
industrialisation to meet competition from the west.

However, there are groups which did raise more substantial critiques of
mainstream Bolshevism. They raised their ideas between 1921 and 1923. How
Lenin and Trotsky responded to them is significant. Rather than embrace them
as expressing what the (according to Leninists) **really** stood for, they
used state repression to break them and they were kicked out of the Communist
Party. All with the approval of Lenin and Trotsky.

The only groups associated with the Bolshevik party which advocated democracy
and freedom for working people were the dissidents of the _"Workers' Truth"_
and _"Workers' Group."_ Material on both is hard to come by. The _"Workers'
Truth"_ group was labelled _"Menshevik"_ by the ruling party while the
_"Workers' Group"_ was dismissed as _"anarcho-syndicalist."_ Both were
expelled from the party and their members arrested by the Bolsheviks. The
latter group is better known than the former and so, by necessity, we will
concentrate on that. It was also the largest, boldest and composed mainly of
workers. We find them labelled the NEP the _"New Exploitation of the
Proletariat"_ and attacking, like the _"Workers' Opposition"_, the _"purely
bureaucratic way"_ industry was run and urging _"the direct participation of
the working class"_ in it. However, unlike the _"Workers' Opposition"_, the
_"Workers' Group"_ extended their call for workers' democracy to beyond the
workplace and party. They wondered if the proletariat might not be _"compelled
once again to start anew the struggle . . . for the overthrow of the
oligarchy."_ They noted that ruling clique in the party _"will tolerate no
criticism, since it considers itself just as infallible as the Pope of Rome."_
[quoted by E.H. Carr, **The Interregnum 1923-1924**, p. 82, p. 269]

The _"Workers' Group"_ is associated with the old worker Bolshevik G. T.
Miasnikov, its founder and leading thinker (see Paul Avrich's essay
**Bolshevik Opposition to Lenin: G. T. Miasnikov and the Workers' Group** for
more details -- any non-attributed quotes can be found in this essay). As Ante
Ciliga recounted in his experiences of political debate in the prison camps in
the late 1920s and early 1930s (ironically, there had always been more freedom
of expression in prison than in Bolshevik society):

> _ "In the criticism of the Lenin of the revolutionary period the tone was
set by . . . the Workers Group . . . [It was], in origin, from the Bolshevik
old guard. But . . . they criticised Lenin's course of action from the
beginning, and not on details but as a whole. The Workers Opposition denounced
Lenin's economic line. The Workers Group went even farther and attacked the
political regime and the single party established by Lenin prior to the NEP .
. . _

>

> _"Having put as the basis of its programme Marx's watchword for the 1st
International -- 'The emancipation of the workers must be the task of the
workers themselves' -- the Workers Group declared war from the start on the
Leninist concept of the 'dictatorship of the party' and the bureaucratic
organisation of production, enunciated by Lenin in the initial period of the
revolution's decline. Against the Leninist line, they demanded organisation of
production by the masses themselves, beginning with factory collectives.
Politically, the Workers Group demanded the control of power and of the party
by the worker masses. These, the true political leaders of the country, must
have the right to withdraw power from any political party, even from the
Communist Party, if they judged that that party was not defending their
interests. Contrary to . . . the majority of the Workers' Opposition, for whom
the demand for 'workers' democracy' was practically limited to the economic
domain, and who tried to reconcile it with the 'single party,' the Workers
Group extended its struggle for workers' democracy to the demand for the
workers to choose among competing political parties of the worker milieu.
Socialism could only be the work of free creation by the workers. While that
which was being constructed by coercion, and given the name of socialism, was
for them nothing but bureaucratic State capitalism from the very beginning."_
[**Op. Cit.**, pp. 277-8]

Years before, Miasnikov had exposed the abuses he has seen first hand under
Lenin's regimed. In 1921, he stated the obvious that _"[i]t stands to reason
that workers' democracy presupposes not only the right to vote but also
freedom of speech and press. If workers who govern the country, manage
factories, do not have freedom of speech, we get a highly abnormal state."_ He
urged total freedom of speech for all. He discussed corruption within the
party, noting that a _"special type of Communist is evolving. He is forward,
sensible, and, what counts most, he knows how to please his superiors, which
the latter like only too much."_ Furthermore, _"[i]f one of the party rank and
file dares to have an opinion of his own, he is looked upon as a heretic and
people scoff at him saying, 'Wouldn't Ilyitch (Lenin) have come to this idea
if it were timely now? So you are the only clever man around, eh, you want to
be wiser than all? Ha, ha, ha! You want to be clever than Ilyitch!' This is
the typical 'argumentation' of the honourable Communist fraternity."_ _"Any
one who ventures a critical opinion of his own,"_ he noted, _"will be labelled
a Menshevik of Social-Revolutionist, with all the consequences that entails."_
[quoted by G. P. Maximoff, **The Guillotine at Work**, p. 269 and p. 268]

Lenin tried to reply to Miasnikov's demand for freedom of speech. Freedom of
the press, Lenin argued, would, under existing circumstances, strengthen the
forces of counter-revolution. Lenin rejected _"freedom"_ in the abstract.
Freedom for whom? he demanded. Under what conditions? For which class? _"We do
not believe in 'absolutes.' We laugh at 'pure democracy,'"_ he asserted.
_"Freedom of press in the RSFSR,"_ Lenin maintained, _"surrounded by bourgeois
enemies everywhere means freedom for the bourgeoisie"_ and as _"we do not want
to commit suicide and that is why we will never do this"_ (i.e. introduce
freedom of speech). According to Lenin, freedom of speech was a _"non-party,
anti-proletarian slogan"_ as well as a _"flagrant political error."_ After
sober reflection, Lenin hoped, Miasnikov would recognise his errors and return
to useful party work.

Miasnikov was not convinced by Lenin's arguments. He drafted a strong reply.
Reminding Lenin of his revolutionary credentials, he wrote: _"You say that I
want freedom of the press for the bourgeoisie. On the contrary, I want freedom
of the press for myself, a proletarian, a member of the party for fifteen
years, who has been a party member in Russia and not abroad. I spent seven and
a half of the eleven years of my party membership before 1917 in prisons and
at hard labour, with a total of seventy-five days in hunger strikes. I was
mercilessly beaten and subjected to other tortures . . . I escaped not abroad,
but for party work here in Russia. To me one can grant at least a little
freedom of press. Or is it that I must leave or be expelled from the party as
soon as I disagree with you in the evaluation of social forces? Such
simplified treatment evades but does not tackle our problems."_ [quoted by
Maximoff, **Op. Cit.**, pp. 270-1] Lenin said, Miasnikov went on, that the
jaws of the bourgeoisie must be cracked:

> _ "To break the jaws of international bourgeoisie, is all very well, but the
trouble is that, you raise your hand against the bourgeoisie and you strike at
the worker. Which class now supplies the greatest numbers of people arrested
on charges of counter-revolution? Peasants and workers, to be sure. There is
no Communist working class. There is just a working class pure and simple."_
[quoted by Maximoff, **Op. Cit.**, p. 271]

_"Don't you know,"_ he asked Lenin, _"that thousands of proletarians are kept
in prison because they talked the way I am talking now, and that bourgeois
people are not arrested on this source for the simple reason that the are
never concerned with these questions? If I am still at large, that is so
because of my standing as a Communist. I have suffered for my Communist views;
moreover, I am known by the workers; were it not for these facts, were I just
an ordinary Communist mechanic from the same factory, where would I be now? In
the Che-Ka [prison] . . . Once more I say: you raise your hand against the
bourgeoisie, but it is I who am spitting blood, and it is we, the workers,
whose jaws are being cracked."_ [quoted by Maximoff, **Ibid.**]

After engaging in political activity in his home area, Miasnikov was summoned
to Moscow and placed under the control of the Central Committee. In defiance
of the Central Committee, he returned to the Urals and resumed his agitation.
At the end of August he appeared before a general meeting of Motovilikha party
members and succeeded in winning them over to his side. Adopting a resolution
against the Orgburo's censure of Miasnikov, they branded his transfer to
Moscow a form of _"banishment"_ and demanded that he be allowed _"full freedom
of speech and press within the party."_

On November 25 he wrote to a sympathiser in Petrograd urging a campaign of
agitation in preparation for the 11th party congress. By now Miasnikov was
being watched by the Cheka, and his letter was intercepted. For Lenin, this
was the last straw: _"We must devote greater attention to Miasnikov's
agitation,"_ he wrote to Molotov on December 5, _"and to report on it to the
Politburo twice a month."_ To deal with Miasnikov, meanwhile, the Orgburo
formed a new commission. This commission recommended his expulsion from the
party, which was agreed by the Politburo on February 20, 1922. This was the
first instance, except for the brief expulsion of S. A. Lozovsky in 1918,
where Lenin actually expelled a well-known Bolshevik of long standing.

By the start of 1923, he had organised a clandestine opposition and formed
(despite his expulsion) the _"Workers' Group of the Russian Communist Party."_
He claimed that it, and not the Bolshevik leadership, represented the
authentic voice of the proletariat. Joining hands in the venture were P. B.
Moiseev, a Bolshevik since 1914, and N. V. Kuznetsov, the former Workers'
Oppositionist. The three men, all workers, constituted themselves as the
_"Provisional Central Organisational Bureau"_ of the group. Their first act,
in February 1923, was to draw up a statement of principles in anticipation of
the Twelfth Party Congress called the "Manifesto of the Workers' Group of the
Russian Communist Party." The manifesto was _"denouncing the New Exploitation
of the Proletariat and urging the workers to fight for soviet democracy,"_
according to Trotskyist historian I. Deutscher. [**The Prophet Unarmed**,
p.107]

The manifesto recapitulated the program of Miasnikov's earlier writings:
workers' self-determination and self-management, the removal of bourgeois
specialists from positions of authority, freedom of discussion within the
party, and the election of new soviets centred in the factories. It protested
against administrative high-handedness, the expanding bureaucracy, the
predominance of non-workers within the party, and the suppression of local
initiative and debate. The manifesto denounced the New Economic Policy (NEP)
as the _"New Exploitation of the Proletariat."_ In spite of the abolition of
private ownership, the worst features of capitalism had been preserved: wage
slavery, differences of income and status, hierarchical authority,
bureaucratism. In the words of the manifesto, the _"organisation of this
industry since the Ninth Congress of the RCP(b) is carried out without the
direct participation of the working class by nominations in a purely
bureaucratic way."_ [quoted by Daniels, **Op. Cit.**, p. 204]

The manifesto wondered whether the Russian proletariat might not be compelled
_"to start anew the struggle -- and perhaps a bloody one -- for the overthrow
of the oligarchy."_ Not that it contemplated an immediate insurrection. Rather
it sought to rally the workers, Communist and non-Communist alike, to press
for the elimination of bureaucratism and the revival of proletarian democracy.
Within the party the manifesto defended-the right to form factions and draw up
platforms. _"If criticism does not have a distinct point of view,"_ Miasnikov
wrote to Zinoviev, _"a platform on which to rally a majority of party members,
on which to develop a new policy with regard to this or that question, then it
is not really criticism but a mere collection of words, nothing but chatter."_
He went even further, calling into question the very Bolshevik monopoly of
power. Under a single-party dictatorship, he argued, elections remained _"an
empty formality."_ To speak of _"workers' democracy"_ while insisting on one-
party government, he told Zinoviev, was to entwine oneself in a contradiction,
a _"contradiction in terms."_

Miasnikov was arrested by the GPU (the new name for the Cheka) on May 25,
1923, a month after the Twelfth Party Congress (the rest of the group's
leadership was soon to follow). Miasnikov was released from custody and
permitted to leave the country and left for Germany (this was a device not
infrequently used by the authorities to rid themselves of dissenters). In
Berlin he formed ties with the council communists of the German Communist
Workers' Party (KAPD) and with the left wing of the German Communist Party.
With the aid of these groups, Miasnikov was able to publish the manifesto of
the Workers' Group, prefaced by an appeal drafted by his associates in Moscow.
The appeal concluded with a set of slogans proclaiming the aims of the
Workers' Group: _"The strength of the working class lies in its solidarity.
Long live freedom of speech and press for the proletarians! Long live Soviet
Power! Long live Proletarian Democracy! Long live Communism!"_

Inside Russia the manifesto was having its effect. Fresh recruits were drawn
into the Workers' Group. It established ties with discontented workers in
several cities and began negotiations with leaders of the now defunct Workers'
Opposition. The group won support within the Red Army garrison quartered in
the Kremlin, a company of which had to be transferred to Smolensk. By summer
of 1923 the group had some 300 members in Moscow, as well as a sprinkling of
adherents in other cities. Many were Old Bolsheviks, and all, or nearly all,
were workers. Soon an unexpected opportunity for the group to extend its
influence arrived. In August and September 1923 a wave of strikes (which
recalled the events of February 1921) swept Russia's industrial centres. An
economic crisis (named the _"scissors' crisis"_) had been deepening since the
beginning of the year, bringing cuts in wages and the dismissal of large
numbers of workers. The resulting strikes, which broke out in Moscow and other
cities, were spontaneous and no evidence existed to connect them with any
oppositionist faction. The Workers' Group, however, sought to take advantage
of the unrest to oppose the party leadership. Stepping up its agitation, it
considered calling a one-day general strike and organising a mass
demonstration of workers, on the lines of Bloody Sunday 1905, with a portrait
of Lenin (rather than the Tzar!) at the lead.

The authorities became alarmed. The Central Committee branded the Workers'
Group as _"anti-Communist and anti-Soviet"_ and ordered the GPU to suppress
it. By the end of September its meeting places had been raided, literature
seized, and leaders arrested. Twelve members were expelled from the party and
fourteen others received reprimands. As one Trotskyist historian put it, the
_"party leaders"_ were _"determined to suppress the Workers' Group and the
Workers' Truth."_ [I. Deutscher, **Op. Cit.**, p. 108] Miasnikov was
considered such a threat that in the autumn of 1923 he was lured back to
Russia on assurances from Zinoviev and Krestinsky, the Soviet ambassador in
Berlin, that he would not be molested. Once in Russia he was immediately
placed behind bars. The arrest was carried out by Dzerzhinsky himself (the
infamous creator and head of the Cheka), a token of the gravity with which the
government viewed the case.

This response is significant, simply because Trotsky was still an influential
member of the Communist Party leadership. As Paul Avrich points out, _"[i]n
January 1924, Lenin died. By then the Workers' Group had been silenced. It was
the last dissident movement within the party to be liquidated while Lenin was
still alive. It was also the last rank-and-file group to be smashed with the
blessing of all the top Soviet leaders, who now began their struggle for
Lenin's mantle."_ [**Bolshevik Opposition To Lenin: G. Miasnikov and the
Workers Group**]

The response of Trotsky is particularly important, given that for most modern
day Leninists he raised the banner of "authentic" Leninism against the obvious
evils of Stalinism. What was his reaction to the state repression of the
Workers' Group? As Deutscher notes, Trotsky _"did not protest when their
adherents were thrown into prison . . . Nor was he inclined to countenance
industrial unrest . . . Nor was he at all eager to support the demand for
Soviet democracy in the extreme form in which the Workers' Opposition and its
splinter groups [like the Workers' Group] had raised it."_ [**Op. Cit.**, pp.
108-9] Dzerzhinsky was given the task of breaking the opposition groups by the
central committee. He _"found that even party members of unquestioned loyalty
regarded them as comrades and refused to testify against them. He then turned
to the Politburo and asked it to declare it was the duty of any party member
to denounce to the GPU people inside the party engaged aggressive action
against the official leaders."_ Trotsky _"did not tell the Politburo plainly
that it should reject Dzerzhinsky's demand. He evaded the question."_ [**Op.
Cit.**, p. 108 and p. 109]

Trotskyist Tony Cliff presents a similar picture of Trotsky's lack of concern
for opposition groups and his utter failure to support working class self-
activity or calls for **real** democracy. He notes that in July and August
1923 Moscow and Petrograd _"were shaken by industrial unrest . . . Unofficial
strikes broke out in many places . . . In November 1923, rumours of a general
strike circulated throughout Moscow, and the movement seems at the point of
turning into a political revolt. Not since the Kronstadt rising of 1921 had
there been so much tension in the working class and so much alarm in the
ruling circles."_ The ruling elite, including Trotsky, acted to maintain their
position and the secret police turned on any political group which could
influence the movement. The _"strike wave gave a new lease of life to the
Mensheviks"_ and so _"the GPU carried out a massive round up of Mensheviks,
and as many as one thousand were arrested in Moscow alone."_ When it was the
turn of the Workers Group and Workers Truth, Trotsky _"did not condemn their
persecution"_ and he _"did not support their incitement of workers to
industrial unrest."_ Moreover, _"[n]or was Trotsky ready to support the demand
for workers' democracy in the extreme form to which the Workers Group and
Workers Truth raised it."_ [**Trotsky**, vol. 3, p. 25, p. 26 and pp. 26-7]

By _"extreme,"_ Cliff obviously means _"genuine"_ as Trotsky did not call for
workers' democracy in any meaningful form. Indeed, his _"New Course
Resolution"_ even went so far as to say that _"it is obvious that there can be
no toleration of the formation of groupings whose ideological content is
directed against the party as a whole and against the dictatorship of the
proletariat. as for instance the Workers' Truth and Workers' Group."_ Trotsky
himself was at pains to distance himself from Myainikov. [**The Challenge of
the Left Opposition (1923-25)**, p. 408 and p. 80] The resolution made it
clear that it considered _"the dictatorship of the proletariat"_ to be
incompatible with **real** workers democracy by arguing _"it is impossible to
tolerate groupings, the ideological contents of which are directed against the
party as a whole and against the dictatorship of the proletariat (such as, for
example, the 'Workers' Truth' and the 'Workers' Group')."_ [quoted by Robert
V. Daniels, **Op. Cit.**, p. 222] Given that both these groups advocated
actual soviet and trade union democracy, the Politburo was simply indicating
that **actual** _"workers' democracy"_ was _"against"_ the dictatorship of the
proletariat (i.e. the dictatorship of the party).

Thus we come to the strange fact that it was Lenin and Trotsky themselves who
knowingly destroyed the groups which represent what modern day Leninists
assert is the _"real"_ essence of Leninism. Furthermore, modern day Leninists
generally ignore these opposition groups when they discuss alternatives to
Stalinism or the bureaucratisation under Lenin. This seems a strange fate to
befall tendencies which, if we take Leninists at their word, expressed what
their tradition stands for. Equally, in spite of their support for party
dictatorship, the _"Workers' Opposition"_ did have some constructive suggests
to make as regards combating the economic bureaucratisation which existed
under Lenin. Yet almost all modern Leninists (like Lenin and Trotsky) dismiss
them as _"syndicalist"_ and utopian. Which is, of course, significant about
the **real** essence of Leninism.

Ultimately, the nature of the various oppositions within the party and the
fate of such real dissidents as the _"Workers' Group"_ says far more about the
real reasons the Russian revolution than most Trotskyist books on the matter.
Little wonder there is so much silence and distortion about these events. They
prove that the _"essence"_ of Bolshevism is not a democratic one but rather a
deeply authoritarian one hidden (at times) behind libertarian sounding
rhetoric. Faced with opposition which were somewhat libertarian, the response
of Lenin and Trotsky was to repress them. In summary, they show that the
problems of the revolution and subsequent civil war did not create but rather
revealed Bolshevism's authoritarian core.

[‹ How did Bolshevik ideology contribute to the failure of the
Revolution?](append44.md "Go to previous page" ) [up](append4.md "Go to
parent page" ) [Why does the Makhnovist movement show there is an alternative
to Bolshevism? ›](append46.md "Go to next page" )

